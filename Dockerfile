﻿# specify the node base image with your desired version node:<version>
FROM node

ENV NODE_VERSION 22.3.0

WORKDIR /usr/src/app

EXPOSE 3000
# Install app dépendencie
COPY package.json ./
RUN npm install

# Bundle app source
COPY . .

CMD npm start