# Hypervisors

Comparative table of virtualization solutions to find a technical alternative to VMware vSphere.

## access to data by filtering

npm is node package manager

### Prerequi

node + npm [Installation](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

### Démarrer web server

Dans une console, dans le repertoire racine du projet : npm start

## Versionning

    V1.0.0 Affichage des différentes solution d'hyperviseurs du marché actuel, passé et future ...
    V1.1.0 Amélioration code source et création d'une image docker et container

## Version docker

Pour obtenir une image(node_container) et un container(hypervisors-of-Nidouille)  : npm run start-container
    
## Ressources

[Comment lire un fichier excel avec javascript](https://www.perplexity.ai/search/Comment-lire-un-5VCFz_GoQNOPSHVvePj_NA)
[GitHub  Nidouille/hypervisors](https://github.com/Nidouille/hypervisors/tree/main)
[express - server web pour node.js](http://expressjs.com/en/starter/installing.html)
