<?php return array(
    'root' => array(
        'name' => 'sir_therry_dean/hypervisors',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'f1c5a14d42bc9f801b8804c0c571e6323e4c73c9',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'phpro/grumphp' => array(
            'dev_requirement' => true,
            'replaced' => array(
                0 => 'v1.13.0',
            ),
        ),
        'phpro/grumphp-shim' => array(
            'pretty_version' => 'v1.13.0',
            'version' => '1.13.0.0',
            'reference' => '973a933d176be41f1196d8db7851e32f985dd798',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../phpro/grumphp-shim',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sir_therry_dean/hypervisors' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'f1c5a14d42bc9f801b8804c0c571e6323e4c73c9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
