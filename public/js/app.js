﻿import Hypervisor from '../js/hypervisor.js'
window.onload = function () {
  console.log("Javascript is OK !")
  const input = document.getElementById('file-input');
  input.addEventListener('change', () => {
    const messages = document.getElementById('messageInfo')
    messages.classList.remove('termine')
    messages.classList.add('encours')
    messages.innerHTML = 'Merci de patienter ! Chargement en cours !'
    const file = input.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = new Uint8Array(e.target.result);
      const workbook = XLSX.read(data, { type: 'array' });
      const sheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[sheetName];
      messages.classList.add("termine")
      messages.classList.remove('encours')
      messages.innerHTML = 'Chargement terminé !'
      let jsonHypervisors = XLSX.utils.sheet_to_json(worksheet)
      // console.log(jsonHypervisors[0])
      const hypervisor = new Hypervisor(jsonHypervisors)    
      hypervisor.afficheHypervisors(hypervisor.hypervisors)
    };

    reader.readAsArrayBuffer(file);
  });
}