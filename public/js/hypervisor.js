﻿export default class Hypervisor {
    attributs = []
    hypervisors = []

    constructor(jsonHypervisors) {
        this.attributs = this.chargementAttributs(jsonHypervisors)
        this.hypervisors = this.chargementHypervisors(jsonHypervisors)
    }

    chargementAttributs(jsonHypervisors) {
        let attributes = jsonHypervisors[0]
        let currentAttributs = []
        Object.keys(attributes).map(function (key, value) {
            currentAttributs.push(attributes[key])
        })
        return currentAttributs
    }

    chargementHypervisors(jsonHypervisors) {
        let currentHypervisors = []
        for (let i = 1; i < jsonHypervisors.length; i++) {
            currentHypervisors.push(jsonHypervisors[i])
        }
        return currentHypervisors
    }

    afficheHypervisors(hypervisors) {
        for (let i = 0; i < hypervisors.length; i++) {
            const currentHypervisor = hypervisors[i]
            this.afficheHypervisor(currentHypervisor, i)
        }
    }

    afficheHypervisor(hypervisor, numero) {
        const container = document.getElementById('container')
        const template = document.getElementById('hypervisors')
        let clone = document.importNode(template.content, true);
        Object.keys(hypervisor).map(function (attribut, value) {
            let idNameAttribut = attribut.replace(/ /g, '_')
            const currentLabel = clone.getElementById(idNameAttribut)
            const currentInput = clone.getElementById('chk_')
            let currentLink = clone.getElementById('Website')           
            if (currentLabel != null) {
                currentLabel.innerHTML = hypervisor[attribut]
                currentLabel.setAttribute('for', 'chk_' + numero)
                currentLabel.setAttribute('id', 'lbtitre_' + numero)
                if (currentInput != null) {
                    currentInput.setAttribute('name', 'chk_' + numero)
                    currentInput.setAttribute('id', 'chk_' + numero)
                }              
            }
            if (currentLink != null && attribut == 'Website') {
                currentLink.setAttribute('href', hypervisor[attribut])
                currentLink.textContent =  hypervisor['Name']
            }
        })
        container.appendChild(clone);
    }
}